#!/usr/bin/env python3
import scrapy
from ..spiders import login
import tempfile
from PIL import Image
import json

class CourseSpider(scrapy.Spider):

    # Spider name
    name = 'course'

    # The first request
    def start_requests(self):
        url = 'https://nportal.ntut.edu.tw/index.do'

        yield scrapy.Request(
            url=url,
            callback=self.get_auth_image
        )

    # Get captcha
    def get_auth_image(self, response):
        captcha_url = 'https://nportal.ntut.edu.tw/authImage.do'
        yield scrapy.Request(
            url=captcha_url,
            callback=self.login
        )
    
    # Start to login
    def login(self, response):
        temp = tempfile.TemporaryFile()
        temp.write(response.body)
        auth_image = Image.open(temp)
        auth_code = login.get_authcode(auth_image)
        global account
        account = '105810037' or input('請輸入帳號：')
        password = 'hacker107' or input('請輸入密碼：')
        form_data = {
            'muid': account,
            'mpassword': password,
            'authcode': auth_code,
            'forceMobile': 'mobile'
        }
        yield scrapy.FormRequest(
            url='https://nportal.ntut.edu.tw/login.do',
            formdata=form_data,
            headers={'Referer':'https://nportal.ntut.edu.tw/index.do'},
            callback=self.login_redirect
        )
    
    # Login redirect to myportal
    def login_redirect(self, response):
        yield scrapy.Request(
            url='https://nportal.ntut.edu.tw/myPortal.do',
            callback=self.aptree_list
        )

    def aptree_list(self, response):
        yield scrapy.Request(
            url='https://nportal.ntut.edu.tw/aptreeList.do',
            callback=self.get_course_auth
        )

    def get_course_auth(self, response):
        yield scrapy.Request(
            url='https://nportal.ntut.edu.tw/ssoIndex.do?apOu=aa_0010-&apUrl=https://aps.ntut.edu.tw/course/tw/courseSID.jsp',
            callback=self.enter_course_system
        )

    def enter_course_system(self, response):
        try:
            form_data = {
                response.css('input::attr(name)').extract()[0]: response.css('input::attr(value)').extract()[0],
                response.css('input::attr(name)').extract()[1]: response.css('input::attr(value)').extract()[1],
                response.css('input::attr(name)').extract()[2]: response.css('input::attr(value)').extract()[2],
                response.css('input::attr(name)').extract()[3]: response.css('input::attr(value)').extract()[3]
            }
        except IndexError:
            print("Login Error")
            return
        yield scrapy.FormRequest(
            url='https://aps.ntut.edu.tw/course/tw/courseSID.jsp',
            formdata=form_data,
            callback=self.course_system
        )
        

    def course_system(self, response):
        yield scrapy.Request(
            url='https://aps.ntut.edu.tw/course/tw/Select.jsp',
            callback=self.course_search
        )

    def course_search(self, response):
        form_data = {
            'code': account,
            'format': '-3'
        }
        yield scrapy.FormRequest(
            url='https://aps.ntut.edu.tw/course/tw/Select.jsp',
            formdata=form_data,
            callback=self.semester_list
        )

    def semester_list(self, response):
        yield scrapy.Request(
            url='https://aps.ntut.edu.tw/course/tw/Select.jsp?format=-2&code={}&year={}&sem={}'.format(account, 106, 2),
            callback=self.course_list
        )

    def course_list(self, response):
        course_list = []
        rows = response.css('tr')
        for row in rows[4:-1]:
            i = 0
            course = {}
            for col in row.css('td'):
                if i == 1:
                    course['name'] = col.css('a::text').extract()
                if i == 6:
                    course['teacher'] = col.css('a::text').extract()
                if i == 15:
                    course['room'] = col.css('a::text').extract()
                i += 1
            course_list.append(course)
        yield {
            'content': str(course_list)
        }